package com.api.application.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.api.application.manager.ApplicationManagerV2;

import generated.ProteinDatabase;
import generated.ProteinDatabase.ProteinEntry;

@Service
public class ApplicationServiceV2 implements ApplicationManagerV2 {

	
	@Autowired
	ApplicationService service;
	
	@Override
	public void parseXML(String url, String fileName, int perPage) {
		// TODO Auto-generated method stub
			int cnt= 0,exported = 0;
			ProteinDatabase newProteinData = new ProteinDatabase();
			
		  try {

				
				 File file = new File("src/main/resources/"+ url + ".xml");
					
				 // set up a StAX reader
				  XMLInputFactory factory = XMLInputFactory.newInstance();
				  
				  // disable doctype
				  factory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
				  
				  XMLStreamReader readerProteinDb = factory.createXMLStreamReader(new FileReader(file));
				  XMLStreamReader reader = factory.createXMLStreamReader(new FileReader(file));

				  //set up JAXB context
				  JAXBContext jaxbContext = JAXBContext.newInstance(ProteinDatabase.class);
				  Unmarshaller um = jaxbContext.createUnmarshaller();
		
				  // get the ProteinDatabase tag
				  JAXBElement<ProteinDatabase> proteinDb =  um.unmarshal(readerProteinDb,ProteinDatabase.class);
				  ProteinDatabase pt = proteinDb.getValue();
				  // set the data to the new object
			 	  newProteinData.setDatabase(pt.getDatabase());
			   	  newProteinData.setDate(pt.getDate());
				  newProteinData.setId(pt.getId());
				  newProteinData.setRelease(pt.getRelease());
				
				  readerProteinDb.close();
				  
				  
			      // read the ProteinEntry only if not found go to next tag
				  while (reader.hasNext() && (!reader.isStartElement() || !reader.getLocalName().equals("ProteinEntry"))) {
	
					  reader.next();
			      }

		
		
				  // loop the ProteinEntry and set to a new ProteinDatabase object
				  while (reader.getEventType() == XMLStreamConstants.START_ELEMENT) {
					  
					  
					  JAXBElement<ProteinEntry> proteinData = um.unmarshal(reader, ProteinEntry.class);
					  ProteinEntry parseP = proteinData.getValue();  
					  
					
					  ProteinEntry p = new ProteinEntry();
					  // set data
					  p.setClassification(parseP.getClassification());
					  p.setGenetics(parseP.getGenetics());
					  p.setHeader(parseP.getHeader());
					  p.setId(parseP.getId());
					  p.setKeywords(parseP.getKeywords());
					  p.setOrganism(parseP.getOrganism());
					  p.setProtein(parseP.getProtein());
					  p.setSequence(parseP.getSequence());
					  p.setSummary(parseP.getSummary());
						
					  
					  newProteinData.getProteinEntry().add(p);
					  
					  cnt++;
					 
					  
					  // export xml file per 100
					  if(cnt % perPage == 0 ) {
						  exported++;
						  service.ConvertObjectToXml(fileName.concat(String.valueOf(exported)),newProteinData);
						  newProteinData.getProteinEntry().clear();
						
					  }


					  if (reader.getEventType() == XMLStreamConstants.CHARACTERS) {
			                reader.next();
			          }
					  
		           }

			        reader.close();
			        
			        System.out.println("No. of files " + exported);
			        
		  	} catch (JAXBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (XMLStreamException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		

		 
		 
		
	}

}
