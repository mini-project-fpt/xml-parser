package com.api.application.services;


import java.io.File;
import java.net.*; 

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.springframework.stereotype.Service;
import com.api.application.manager.ApplicationManager;

import generated.ProteinDatabase;
import generated.ProteinDatabase.ProteinEntry;

@Service
public class ApplicationService implements ApplicationManager {


	@Override
	public void extractXML(String url, String fileName, int perPage) {
		
		// call the xml to object parser
		ProteinDatabase proteinData = convertXMLtoObject(url);
		
		
		ProteinDatabase newProteinData = new ProteinDatabase();
		
		
		int count = 0,exported = 0;
		// set the data to the new object
		newProteinData.setDatabase(proteinData.getDatabase());
		newProteinData.setDate(proteinData.getDate());
		newProteinData.setId(proteinData.getId());
		newProteinData.setRelease(proteinData.getRelease());
					
		
		// loop
		for(ProteinEntry t : proteinData.getProteinEntry()) {
			
			
			ProteinDatabase.ProteinEntry p = new ProteinDatabase.ProteinEntry();
			// set data
			p.setClassification(t.getClassification());
			p.setGenetics(t.getGenetics());
			p.setHeader(t.getHeader());
			p.setId(t.getId());
			p.setKeywords(t.getKeywords());
			p.setOrganism(t.getOrganism());
			p.setProtein(t.getProtein());
			p.setSequence(t.getSequence());
			p.setSummary(t.getSummary());
			
			// add to the new list of Protein
			newProteinData.getProteinEntry().add(p);

		
			count++;
			
			// export the data per 5 entries
			if(count % perPage == 0){
				exported++;
				ConvertObjectToXml(fileName.concat(String.valueOf(exported)),newProteinData);
				newProteinData.getProteinEntry().clear();
			}
			
		}
		
		  System.out.println("No. of files " + exported);
		
	}
	
	// convert object to xml
	public void  ConvertObjectToXml(String fileName, ProteinDatabase proteinData) {
		try {
	
			File file = new File("src/main/resources/splittedXML/"+ fileName + ".xml");
			JAXBContext jaxbContext = JAXBContext.newInstance(ProteinDatabase.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	
			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	
			jaxbMarshaller.marshal(proteinData, file);
			//jaxbMarshaller.marshal(proteinData, System.out);
	
	      } catch (JAXBException e) {
	    	  e.printStackTrace();
	      }
	
				
			
	}
	
	// convert xml to object
	private ProteinDatabase convertXMLtoObject(String fileName) {
	
		ProteinDatabase p = new ProteinDatabase();
		try {
			// convert the xml  to object
			File file = new File("src/main/resources/"+ fileName + ".xml");
			
		 	//URL file = new URL(fileName);
			JAXBContext jaxbContext = JAXBContext.newInstance(ProteinDatabase.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			p = (ProteinDatabase) jaxbUnmarshaller.unmarshal(file);
		
			
			
	      } catch (JAXBException e) {
	    	  e.printStackTrace();
//		} catch (MalformedURLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
		}
		
		return p;
	} 
	

}
