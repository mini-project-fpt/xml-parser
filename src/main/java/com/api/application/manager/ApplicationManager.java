package com.api.application.manager;

import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationManager {
	void extractXML(String url, String fileName, int perPage);
}
