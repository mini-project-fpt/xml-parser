package com.api.application.manager;

import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationManagerV2 {
	void  parseXML(String url, String fileName, int perPage);
}
