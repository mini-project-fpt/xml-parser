package com.api.application.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class ParseDTO {
	
	@NotNull
	private String xmlFileSrc;
	
	@NotNull
	private String fileNametoSave;
	
	@NotNull
	@Min(value = 5, message = "must be greater than equal 5")
	private int noOfRows;

	//constructor
	public ParseDTO(@NotNull String xmlFileSrc, @NotNull String fileNametoSave,
			@NotNull @Min(value = 5, message = "must be greater than equal 5") int noOfRows) {
		this.xmlFileSrc = xmlFileSrc;
		this.fileNametoSave = fileNametoSave;
		this.noOfRows = noOfRows;
	}

	
	// getters and setters
	public String getXmlFileSrc() {
		return xmlFileSrc;
	}

	public void setXmlFileSrc(String xmlFileSrc) {
		this.xmlFileSrc = xmlFileSrc;
	}

	public String getFileNametoSave() {
		return fileNametoSave;
	}

	public void setFileNametoSave(String fileNametoSave) {
		this.fileNametoSave = fileNametoSave;
	}

	public int getNoOfRows() {
		return noOfRows;
	}

	public void setNoOfRows(int noOfRows) {
		this.noOfRows = noOfRows;
	}


	@Override
	public String toString() {
		return "ParseDTO [xmlFileSrc=" + xmlFileSrc + ", fileNametoSave=" + fileNametoSave + ", noOfRows=" + noOfRows
				+ "]";
	}
	

	
}
