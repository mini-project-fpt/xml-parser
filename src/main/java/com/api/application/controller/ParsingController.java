package com.api.application.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.application.dto.ParseDTO;
import com.api.application.manager.ApplicationManager;
import com.api.application.manager.ApplicationManagerV2;


@RestController
@RequestMapping("/api")
public class ParsingController {

	@Autowired
	ApplicationManager service;

	@Autowired
	ApplicationManagerV2 service2;
	
	@PostMapping("/proteinV1")
	public ResponseEntity<String> getAll(@Valid @RequestBody ParseDTO parseDto ) {
		
		service.extractXML(parseDto.getXmlFileSrc(),parseDto.getFileNametoSave(), parseDto.getNoOfRows());
		return ResponseEntity.status(HttpStatus.OK)
					         .body("Successfully parsed");
	}
	
	
	@PostMapping("/proteinV2")
	public ResponseEntity<String> getAllV2(@Valid @RequestBody ParseDTO parseDto ) {
		
		service2.parseXML(parseDto.getXmlFileSrc(),parseDto.getFileNametoSave(),parseDto.getNoOfRows());
		return ResponseEntity.status(HttpStatus.OK)
					         .body("Successfully parsed");
	}
	


	
}
