# Project Title

XML Parser


## Getting Started

XML parser is a mini project implemented using java (spring-boot).The project can be run in local.

### Details
 
```
 * The xml file to be parse should be put on this folder.
   src-> main-> resources
  
 * Splitted files can be located on this folder.
   src-> main -> resources-> splittedXML
   
 * Schema/xsd file should be put on this folder.
   src-> main -> resources 
 
 To run the application
```

### API
 
```
	In my case, the tomcat running on localhost:8080
	http://localhost:8080/

    * POST: /api/proteinV1 -> parsing xml version 1
	
		sample valid json :
		{
			"xmlFileSrc": "psd7003_pv",
			"fileNametoSave": "v1File",
			"noOfRows" : 5
		}

	* POST: /api/proteinV2 -> parsing xml version 2
	
		sample valid json :
		
		
		{
			"xmlFileSrc": "psd7003",
			"fileNametoSave": "v2File",
			"noOfRows" : 100
		}

	
	Note : 
	Check the file to be parse should be located on specific folder.
	
```

### Running the application

```
 1. Schema and xml files to be parse should be put on the specific folder.
 2 .To generate the ProteinDatabase using the provided schema
	run this command : 
	mvn clean install
 3.run this command : 
   mvn spring-boot:run
 4.use postman or any tool to test the API

sample : 
http://localhost:8080/api/proteinV1
	
  
```

